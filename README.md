# Opitaan yhdessä Javascriptiä!

[![Join the chat at https://gitter.im/javascript-finland/opitaanyhdessa-javascript](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/javascript-finland/opitaanyhdessa-javascript?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Opitaan Javascriptia yhdessä! Jaetaan hyviä materiaaleja, tule jutustelemaan Gitteriin.

## Miksi?

Internet on pullollaan mitä erilaisempia Javascript oppaita, tutoriaaleja. Vähennetään "virheaskeleiden" määrää ja kynnystä Javascriptin opetteluun. Suosittuja frameworkkeja, hyödyllisiä lisäkirjastoja kategorisoituna käyttötarkoituslähtöiseen hakuun perustuen.

## Miten?

1. Ideoiden kerääminen Githubiin. Tiedätkö työkalua, joka soveltuisi ideoiden keräämiseen ja integroituisi hyvin?
2. Tehdään ideoista tehtävät Issue Trackeriin.
3. API materiaalien lisäämiseen. Speksaus Swagger työkalulla, http://editor.swagger.io/ API nopeasti pystyyn Loopback frameworkilla http://loopback.io/
4. Ensimmäinen versio käyttöliittymästä. Toteutus AngularJS, https://angularjs.org/
5. Tärkein ominaisuus tulee olemaan tehokas interaktiivinen haku, tässä kohtaa panostetaan siihen. http://www.opensemanticsearch.org/
6. Markkinointi, testaus, palaute.
7. Viedään oppia muihin projekteihin: EduCloud Alliance, API:Suomi
8. MOTTO: "Pala kerrallaan, iteroi, jaa tuloksia."

Suosittuja frameworkkeja, hyödyllisiä lisäkirjastoja kategorisoituna tehokkaaseen, interaktiiviseen käyttötarkoituslähtöiseen hakuun perustuen, esim. "What you want to do?" Ennaltamääritelty tagivalikoima linkitettynä kategorioihin. 

## Tieteellinen taikina aka Tutkimusarvo

### Oppivan haun analysointi ja kehittäminen
Ehdotetaan valmiita tageja, mutta mahdollisuus käyttää omiakin. Uudet tagit suodatetaan tietokantaan, aika ajoin käydään läpi ja linkitetään sopiviin kategorioihin, jolloin tagivalikoima laajenee käytön myötä vastaamaan luontaista käyttöä. Pois manuaalisesta! Tietyin ajanjaksoin skripti käy kannan läpi, lisätään ominaisuuksia data-analyysin pohjalta. Koneoppimisen hyödyntäminen?

## Kolme kultaharkkoa aka Yritysarvo

* Lisätään tietoisuutta data-analyysin käytöstä reaalimaailman käyttökontekstissa
* ISON datan työkalupakin löytäminen, ja sen kehittäminen
* Konseptin hahmottaminen ISON datan tuotteistamiseen yrityskäyttöön

## Rahoitus
Neuvoja kaivataan! Tule Gitteriin!

## Ympäristö
* "Tiedekummi", "Koodikummi" -> parantaa projektin onnistumismahdollisuuksia
* Demola vai Yliopisto-tutkimusprojekti, näiden yhdistelmä?
